import React from 'react';

export class Button extends React.Component{

    render(){

        return(
            <button color="blue" shadowSize={2}>{this.props.name}</button>
        );
    }
}

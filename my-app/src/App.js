import React from 'react';
import {Button} from './Button';
import { PostForm } from './PostForm';



function App() {
  return (
    <div className="App">
      <header style = {{fontSize: 50,color:'blue',textAlignVertical:'centre',textAlign:'centre'}} className="App-header">
        Nigel's Blogspot
      </header>


      <Button name = 'Posts'/><Button name = 'Login'/>
      <PostForm />

    </div>
  );
}

export default App;
